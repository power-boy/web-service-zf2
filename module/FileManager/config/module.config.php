<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /file-manager/:controller/:action
            'file-manager' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/file-manager[/:cd]',
                    'constraints' => array(
                        'cd' => '.*'
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'FileManager\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                        ),
                    ),
                ),
            ),
            'file-manager-signout' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/file-manager/signout',
                    'defaults' => array(
                        'controller' => 'FileManager\Controller\Index',
                        'action'     => 'signout',
                    ),
                ),
            ),
            'login' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/login',
                    'defaults' => array(
                        'controller' => 'FileManager\Controller\Index',
                        'action'     => 'login',
                    ),
                ),
            ),
            'upload' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/file-manager/upload',
                    'defaults' => array(
                        'controller' => 'FileManager\Controller\FileManager',
                        'action'     => 'upload',
                    ),
                ),
            ),
            'delete' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/file-manager/delete[/:id]',
                    'defaults' => array(
                        'controller' => 'FileManager\Controller\FileManager',
                        'action'     => 'delete',
                    ),
                    'constraints' => array(
                        'id'    => '.*'
                    ),
                ),
            ),
            'download' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/file-manager/download[/:id]',
                    'defaults' => array(
                        'controller' => 'FileManager\Controller\FileManager',
                        'action'     => 'download',
                    ),
                    'constraints' => array(
                        'id'    => '.*'
                    ),
                ),
            ),
            'create-dir' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/create-dir[/:name]',
                    'defaults' => array(
                        'controller' => 'FileManager\Controller\FileManager',
                        'action'     => 'createDir',
                    ),
                    'constraints' => array(
                        'name'    => '.*'
                    ),
                ),
            ),

        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'FileManager\Controller\Index' => 'FileManager\Controller\IndexController',
            'FileManager\Controller\FileManager' => 'FileManager\Controller\FileManagerController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'file-manager/index/index' => __DIR__ . '/../view/file-manager/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'auth' => array(
        'facebook' => array(
            'appId' => '731672823556706',
            'secret'=> '717f072f92c814a432c26983199b9775',
            'redirectUri'=> 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/login'
        )
    ),
);
