<?php
/**
 * Taras Seryogin
 */
namespace FileManager\Validator;

use Zend\Validator\AbstractValidator;

class FolderNameValidator extends AbstractValidator
{
    const FOLDER_NAME  = 'folder';
    protected $messageTemplates = array(
        self::FOLDER_NAME => "'%value%' is not valid. Folder must be contain only latin characters",
    );
    public function isValid($value)
    {
        $this->setValue($value);

        if (!preg_match('/^[a-z0-9- ]+$/i', $value)) {
            $this->error(self::FOLDER_NAME);
            return false;
        }

        return true;
    }
}
