<?php
/**
 * Signup Filter
 *
 * Taras Seryogin
 */
namespace FileManager\Form;

use FileManager\Validator\FolderNameValidator;
use Zend\InputFilter\InputFilter;

class FolderNameFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
               new FolderNameValidator()
            ),
        ));

    }
}
