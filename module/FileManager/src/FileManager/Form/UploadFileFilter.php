<?php
/**
 * Signup Filter
 *
 * Taras Seryogin
 */
namespace FileManager\Form;

use Zend\InputFilter\InputFilter;

class UploadFileFilter extends InputFilter
{
    public function __construct($maxSize = null)
    {
        $this->add(array(
            'name' => 'file',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'FileSize',
                    'options' => array(
                        'min' => '0kB',
                        'max' => $maxSize,
                    ),
                ),
            )
        ));

        $this->add(array(
            'name' => 'uploadDir',
            'required'    => true,
            'allow_empty' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
        ));

    }
}
