<?php

namespace FileManager\Service;

use FileManager\Model\DB;
use FileManager\Model\FileSystem;

class ServiceFactory
{
    public function create($type)
    {
        if ($type == 'file_system') {
            return new FileSystem();
        }
        if ($type == 'db') {
            return new DB();
        }
    }
}
