<?php

namespace FileManager\Service;

interface ServiceInterface
{
    public function upload($config, $data, $dbAdapter);
    public function download($config, $file, $dbAdapter);
    public function delete($config, $file, $dbAdapter);
    public function createDir($config, $file, $dbAdapter);
    public function showFiles($config, $file, $dbAdapter);
}
