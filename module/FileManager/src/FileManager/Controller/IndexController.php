<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace FileManager\Controller;

use FileManager\Service\ServiceFactory;
use Zend\Db\TableGateway\TableGateway;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use FileManager\Model\UsersTable;
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;

/**
 * Class IndexController
 *
 * @method Request getRequest()
 * @package FileManager\Controller
 */
class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $userSession = new Container('user');
        $userlogin = $userSession->offsetGet('login');
        $userId = $userSession->offsetGet('userId');
        if ($userlogin) {
            $sm = $this->getServiceLocator();
            $config = $sm->get('Config');
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $factory = new ServiceFactory();
            $adapter = $factory->create($config['file_manager']['adapter']);
            $uploadDir = $config['file_manager']['dir'] .'/'. $userId;
            $dir = PATH_PUBLIC .'/'. $uploadDir;
            if (!is_dir($dir)) {
                mkdir($dir, 0777, true);
            }
            $subDir = urldecode($this->getEvent()->getRouteMatch()->getParam('cd'));
            $subDir = ltrim($subDir, '/');
            $files = $adapter->showFiles($uploadDir, $subDir, $dbAdapter);
            if ($files == 'error') {
                return $this->redirect()->toRoute('file-manager');
            }
            $returnBackUri = $adapter->getGoBackUri($subDir);
            $response = array(
                'page' => '',
                'files' => $files,
                'returnBackUri' => $returnBackUri,
                'subDir' => $subDir
            );

            return new ViewModel(array(
                'response' => json_encode($response),
                'login' => $userlogin
            ));
        }
    }

    public function loginAction()
    {
        $sessionManager = new SessionManager();
        $sessionManager->start();
        $config = $this->getServiceLocator()->get('Config');
        $options = $config['auth']['facebook'];
        // init app with app id and secret
        FacebookSession::setDefaultApplication($options['appId'], $options['secret']);
        // login helper with redirect_uri
        $helper = new FacebookRedirectLoginHelper($config['auth']['facebook']['redirectUri']);

        try {
            $session = $helper->getSessionFromRedirect();
        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
            // print_r($ex);
        } catch(Exception $ex) {
            // print_r($ex);
        }

        // see if we have a session
        if (isset($session)) {
            // graph api request for user data
            $request = new FacebookRequest($session, 'GET', '/me');
            $response = $request->execute();
            $dataFromFb = $response->getResponse();

            $sm = $this->getServiceLocator();
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');

            $tableGateway = new TableGateway('auth', $dbAdapter);
            $authTable = new UsersTable($tableGateway);

            $tableGateway = new TableGateway('users', $dbAdapter);
            $userTable = new UsersTable($tableGateway);

            $checked = $authTable->getAuthUser($dataFromFb->id);
            if ($checked) {
                $user = $userTable->getUser($checked->userId);
                $userSession = new Container('user');
                $userSession->login = $user->login;
                $userSession->userId = $checked->userId;
            } else {
                $userId = $userTable->saveUser($dataFromFb);
                $authTable->saveAuthData($userId, $dataFromFb);
                $userSession = new Container('user');
                $userSession->login = $dataFromFb->name;
                $userSession->userId = $userId;
            }

            $model = new ViewModel(array(
                'login' => $userSession->login
            ));
            $model->setTemplate('file-manager/index/index');
            return $this->redirect()->toRoute('file-manager');

        } else {
            $link = $helper->getLoginUrl(array('email'));
            return $this->redirect()->toUrl($link);
        }
    }


    public function signoutAction()
    {
        $userSession = new Container('user');
        $userSession->offsetUnset('login');
        return $this->redirect()->toRoute('file-manager');
    }
}
