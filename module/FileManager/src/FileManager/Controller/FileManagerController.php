<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace FileManager\Controller;

use FileManager\Service\ServiceFactory;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Request;

/**
 * Class FileManagerController
 *
 * @method Request getRequest()
 * @package FileManager\Controller
 */
class FileManagerController extends AbstractActionController
{
    public function uploadAction()
    {
        $factory = new ServiceFactory();
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $adapter = $factory->create($config['file_manager']['adapter']);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            return $adapter->upload($config, $post, $dbAdapter);
        }
    }

    public function downloadAction()
    {
        $file = urldecode($this->getEvent()->getRouteMatch()->getParam('id'));
        $factory = new ServiceFactory();
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $adapter = $factory->create($config['file_manager']['adapter']);
        return $adapter->download($config, $file, $dbAdapter);
    }

    public function deleteAction()
    {
        $file = urldecode($this->getEvent()->getRouteMatch()->getParam('id'));
        $factory = new ServiceFactory();
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $adapter = $factory->create($config['file_manager']['adapter']);
        $path = $adapter->delete($config, $file, $dbAdapter);
        return $this->redirect()->toRoute('file-manager', array('cd' => urlencode($path)));
    }

    public function createDirAction()
    {
        $folderName = $this->getEvent()->getRouteMatch()->getParam('name');
        $factory = new ServiceFactory();
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
        $adapter = $factory->create($config['file_manager']['adapter']);
        $path = $adapter->createDir($config, $folderName, $dbAdapter);
        return $this->redirect()->toRoute('file-manager', array('cd' => urlencode($path)));
    }
}
