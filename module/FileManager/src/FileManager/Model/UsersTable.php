<?php

namespace FileManager\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Di\ServiceLocator;

class UsersTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveUser($dataFromFb)
    {
        $data = array(
            'email' => $dataFromFb->email,
            'login' => $dataFromFb->name,
            'status' => 'active',
        );
        $this->tableGateway->insert($data);
        return $this->tableGateway->lastInsertValue;
    }

    public function saveAuthData($userId, $dataFromFb)
    {
        $data = array(
            'foreignKey' => $dataFromFb->id,
            'userId' => $userId,
            'provider' => 'facebook',
            'token' => 0,
            'tokenSecret' => 0,
            'tokenType' => 'access'
        );
        $this->tableGateway->insert($data);
        return $this->tableGateway->lastInsertValue;
    }

    public function getAuthUser($id)
    {
        $rowset = $this->tableGateway->select(array('foreignKey' => $id));
        return $rowset->current();
    }

    public function getUser($id)
    {
        $rowset = $this->tableGateway->select(array('id' => $id));
        return $rowset->current();
    }
}
