<?php

namespace FileManager\Model;

use FileManager\Form\FolderNameFilter;
use FileManager\Service\ServiceInterface;
use FileManager\Form\UploadFileFilter;
use Zend\Filter\File\RenameUpload;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class FileSystem extends AbstractActionController implements ServiceInterface
{
    public function upload($config, $data, $dbAdapter)
    {
        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');
        $uploadDir = $config['file_manager']['dir'] .'/'. $userId;
        $inputFilter = new UploadFileFilter($config['file_manager']['maxSize']);
        $inputFilter->setData($data);
        if ($inputFilter->isValid()) {
            $data = $inputFilter->getValues();
            if ($data['uploadDir']) {
                $target = PATH_PUBLIC .'/'. $uploadDir .'/'. $data['uploadDir'];
            } else {
                $target = PATH_PUBLIC .'/'. $uploadDir;
            }
            $filter = new RenameUpload(array(
                "target"    => $target,
                'overwrite' => false,
                'use_upload_name' => true,
                'use_upload_extension' => true,
                'randomize' => true,
            ));
            $newFile = $filter->filter($data['file']);
            $tempArr = explode('/', $newFile['tmp_name']);
            $fileName = array_pop($tempArr);
            $pos = strpos($newFile['tmp_name'], '/upload');
            $piece = substr($newFile['tmp_name'], $pos);
            $res = explode('/', $piece);
            unset($res[0], $res[1], $res[2]);
            $location = implode('/', $res);
            $filesize = round($newFile['size'] / 1024) . 'kB';
            return new JsonModel(array(
                'file' => array(
                    'name' => $fileName,
                    'type' => $newFile['type'],
                    'actionlocation' => $location,
                    'previewlocation' => $piece,
                    'size' => $filesize,
                )
            ));
        } else {
            $response = $this->getResponse();
            $response->setContent(
                'Upload data isn\'t valid'
            );
            $response->setStatusCode(400);
            return $response;
        }
    }

    public function download($config, $file, $dbAdapter)
    {
        $separatorPos = strrpos($file, '.ext');
        $rawPath = substr($file, 0, $separatorPos);

        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');

        $parsedArr = explode('/', $rawPath);
        $fileName = array_pop($parsedArr);
        $filePath = PATH_PUBLIC .'/'. $config['file_manager']['dir'] .'/'. $userId .'/'. $rawPath;

        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($filePath, 'r'));
        $response->setStatusCode(200);

        $headers = new \Zend\Http\Headers();
        $headers->addHeaderLine('Content-Type', 'application/octet-stream')
            ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $fileName . '"');

        $response->setHeaders($headers);
        return $response;
    }

    public function delete($config, $file, $dbAdapter)
    {
        $separatorPos = strrpos($file, '.ext');
        $rawPath = substr($file, 0, $separatorPos);

        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');

        $parsedArr = explode('/', $rawPath);
        $fileName = array_pop($parsedArr);
        $filePath = PATH_PUBLIC .'/'. $config['file_manager']['dir'] .'/'. $userId .'/'. $rawPath;
        if (is_dir($filePath)) {
            $this->flashMessenger()
                ->setNamespace('info')
                ->addMessage('Folder deleted successfully');
        } else {
            $this->flashMessenger()
                ->setNamespace('info')
                ->addMessage('File deleted successfully');
        }

        $parsedArr = implode('/', $parsedArr);
        system('rm -rf ' . escapeshellarg($filePath));
        return $parsedArr;
    }

    public function createDir($config, $file, $dbAdapter)
    {
        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');
        $output = htmlspecialchars($file, ENT_QUOTES);
        $inputFilter = new FolderNameFilter();
        $parsedArr = explode('/', $output);
        $dirName = array_pop($parsedArr);
        $parentDir = implode('/', $parsedArr);
        $inputFilter->setData(array('name' => $dirName));
        if ($inputFilter->isValid()) {
            $dirPath = PATH_PUBLIC .'/'. $config['file_manager']['dir'] .'/'. $userId .'/'. $output;
            if (!is_dir($dirPath)) {
                mkdir($dirPath, 0777, true);
                $this->flashMessenger()
                    ->setNamespace('success')
                    ->addMessage('Directory is created successfully');
            } else {
                $this->flashMessenger()
                    ->setNamespace('error')
                    ->addMessage('Folder with the same name already exists');
            }
        } else {
            $this->flashMessenger()
                ->setNamespace('error')
                ->addMessage(current($inputFilter->getMessages('name')));
        }
        return $parentDir;
    }

    public function showFiles($uploadDir, $subDir = null, $dbAdapter)
    {
        if ($subDir) {
            $path = PATH_PUBLIC .'/'. $uploadDir .'/'. $subDir;
        } else {
            $path = PATH_PUBLIC .'/'. $uploadDir;
        }
        $files = array();
        if (is_dir($path)) {
            $handle = opendir($path);
            if ($handle) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry != "." && $entry != "..") {
                        $files[$entry] = $this->fileInfo($path .'/'. $entry);
                    }
                }
                closedir($handle);
                return $files;
            }
        } else {
            $this->flashMessenger()
                ->setNamespace('error')
                ->addMessage('Sorry, but this folder is not exist');
            return 'error';
        }
    }

    public function fileInfo($path)
    {
        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $mimetype = $finfo->file($path);
        $pos = strpos($path, 'upload/');
        $fileLocation = substr($path, $pos);
        $res = explode('/', $fileLocation);
        unset($res[0], $res[1]);
        $name = implode('/', $res);
        $filesize = round(filesize($path) / 1024) .'kB';
        if (is_dir($path)) {
            $file = array(
                'location' => $name,
                'mimetype' => $mimetype
            );
        } else {
            $file = array(
                'location' => '/'. $fileLocation,
                'actionLocation' => '/'. $name,
                'filesize' => $filesize,
                'mimetype' => $mimetype,
                'name' => $name,
            );
        }
        return $file;
    }

    public function getGoBackUri($subDir)
    {
        if (!$subDir) {
            $returnBackUri = '';
        } else {
            $path = explode('/', $subDir);
            if (count($path) == 1) {
                $returnBackUri = 'file-manager';
            } else {
                $hold = array_pop($path);
                $returnBackUri = implode('/', $path);
            }
        }
        return $returnBackUri;
    }
}
