<?php

namespace FileManager\Model;

use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;
use Zend\Di\ServiceLocator;

class MediaTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function deleteByPath($path)
    {
        $sqlSelect = $this->tableGateway->getSql()->delete();
        $sqlSelect->where->like('path', $path .'%');
        $rowset = $this->tableGateway->deleteWith($sqlSelect);
        return $rowset;
    }

    public function deleteById($id)
    {
        $rowset = $this->tableGateway->delete(array('id' => $id));
        return $rowset;
    }

    public function getAllByUserId($userId)
    {
        $rowset = $this->tableGateway->select(array('userId' => $userId));
        return $rowset->current();
    }

    public function getFilesByPathInCurrentBranch($path, $userId)
    {
        $sqlSelect = $this->tableGateway->getSql()->select()->columns(array('name'));
        $sqlSelect->where->like('path', $path .'%');
        $sqlSelect->where(array('userId' => $userId));
        $rowset = $this->tableGateway->selectWith($sqlSelect);
        return $rowset->toArray();
    }

    public function getFilesByPath($path, $userId)
    {
        $rowset = $this->tableGateway->select(array('path' => $path, 'userId' => $userId));
        return $rowset->toArray();
    }

    public function getFileByNameAndPath($name, $path, $userId)
    {
        $rowset = $this->tableGateway->select(array('name' => $name, 'path' => $path, 'userId' => $userId));
        return $rowset->current();
    }

    public function saveFile($file)
    {
        $this->tableGateway->insert($file);
        return $this->tableGateway->lastInsertValue;
    }
}
