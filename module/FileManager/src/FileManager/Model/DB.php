<?php

namespace FileManager\Model;

use FileManager\Form\FolderNameFilter;
use FileManager\Form\UploadFileFilter;
use FileManager\Service\ServiceInterface;
use Zend\Db\TableGateway\TableGateway;
use Zend\Filter\File\RenameUpload;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

class DB extends AbstractActionController implements ServiceInterface
{
    public function upload($config, $data, $dbAdapter)
    {
        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');
        $uploadDir = $config['file_manager']['dir'] .'/'. $userId;
        $inputFilter = new UploadFileFilter($config['file_manager']['maxSize']);
        $inputFilter->setData($data);
        if ($inputFilter->isValid()) {
            $data = $inputFilter->getValues();
            $filter = new RenameUpload(array(
                "target"    => PATH_PUBLIC .'/'. $uploadDir,
                'overwrite' => false,
                'use_upload_name' => true,
                'use_upload_extension' => true,
                'randomize' => true,
            ));
            $newFile = $filter->filter($data['file']);
            $tempArr = explode('/', $newFile['tmp_name']);
            $fileName = array_pop($tempArr);
            $size = round($newFile['size'] / 1024) . 'kB';
            $tableGateway = new TableGateway('media', $dbAdapter);
            $mediaTable = new MediaTable($tableGateway);
            if ($data['uploadDir']) {
                $path = '/'. $data['uploadDir'];
                $actionlocation = $data['uploadDir'] .'/'. $fileName;
            } else {
                $path = '/';
                $actionlocation = $fileName;
            }
            $previewlocation = '/'. $uploadDir .'/'. $fileName;
            $name = $mediaTable->getFileByNameAndPath($fileName, $path, $userId);
            if ($name) {
                $response = $this->getResponse();
                $response->setContent(
                    'File with the same name already exists'
                );
                $response->setStatusCode(400);
                return $response;
            } else {
                $file = array(
                    'name' => $fileName,
                    'path' => $path,
                    'userId' => $userId,
                    'type' => $newFile['type'],
                    'size' => $size
                );
                $mediaTable->saveFile($file);
                return new JsonModel(array(
                    'file' => array(
                        'name' => $fileName,
                        'type' => $newFile['type'],
                        'actionlocation' => '/'. $actionlocation,
                        'previewlocation' => $previewlocation,
                        'size' => $size
                    )
                ));
            }
        } else {
            $response = $this->getResponse();
            $response->setContent(
                'Upload data isn\'t valid'
            );
            $response->setStatusCode(400);
            return $response;
        }
    }

    public function download($config, $file, $dbAdapter)
    {
        $separatorPos = strrpos($file, '.ext');
        $rawPath = substr($file, 0, $separatorPos);

        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');

        $parsedArr = explode('/', $rawPath);
        $fileName = array_pop($parsedArr);
        $path =  implode('/', $parsedArr);
        if (count($parsedArr) == 1 || empty($parsedArr)) {
            $path = '/';
        }
        $tableGateway = new TableGateway('media', $dbAdapter);
        $mediaTable = new MediaTable($tableGateway);

        $name = $mediaTable->getFileByNameAndPath($fileName, $path, $userId);
        if ($name) {
            $filePath = PATH_PUBLIC .'/'. $config['file_manager']['dir'] .'/'. $userId .'/'. $fileName;

            $response = new \Zend\Http\Response\Stream();
            $response->setStream(fopen($filePath, 'r'));
            $response->setStatusCode(200);

            $headers = new \Zend\Http\Headers();
            $headers->addHeaderLine('Content-Type', 'application/octet-stream')
                ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $fileName . '"');

            $response->setHeaders($headers);
            return $response;
        }
    }

    public function delete($config, $file, $dbAdapter)
    {
        $separatorPos = strrpos($file, '.ext');
        $rawPath = substr($file, 0, $separatorPos);

        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');

        $parsedArr = explode('/', $rawPath);
        $fileName = array_pop($parsedArr);
        $path =  implode('/', $parsedArr);
        if (count($parsedArr) == 1 || empty($parsedArr)) {
            $path = '/';
        }
        $tableGateway = new TableGateway('media', $dbAdapter);
        $mediaTable = new MediaTable($tableGateway);
        $name = $mediaTable->getFileByNameAndPath($fileName, $path, $userId);
        if ($name) {
            if ($name->type == 'directory') {
                $filesInDirectory = $mediaTable->getFilesByPathInCurrentBranch($rawPath, $userId);
                foreach ($filesInDirectory as $file) {
                    $filePath = PATH_PUBLIC .'/'. $config['file_manager']['dir'] .'/'. $userId .'/'. $file['name'];
                    system('rm -rf ' . escapeshellarg($filePath));
                }
                $mediaTable->deleteById($name->id);
                $mediaTable->deleteByPath($rawPath);
                $this->flashMessenger()
                    ->setNamespace('info')
                    ->addMessage('Folder deleted successfully');
            } else {
                $mediaTable->deleteById($name->id);
                $this->flashMessenger()
                    ->setNamespace('info')
                    ->addMessage('File deleted successfully');
                $filePath = PATH_PUBLIC .'/'. $config['file_manager']['dir'] .'/'. $userId .'/'. $name->name;
                system('rm -rf ' . escapeshellarg($filePath));
            }
        }
        return $path;
    }

    public function createDir($config, $file, $dbAdapter)
    {
        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');
        $output = htmlspecialchars($file, ENT_QUOTES);
        $inputFilter = new FolderNameFilter();
        $parsedArr = explode('/', $output);
        $dirName = array_pop($parsedArr);
        $parentDir = implode('/', $parsedArr);
        $inputFilter->setData(array('name' => $dirName));
        if ($inputFilter->isValid()) {
            if (empty($parentDir)) {
                $path = '/';
            } else {
                $path = '/'. $parentDir;
            }
            $file = array(
                'name' => $dirName,
                'path' => $path,
                'userId' => $userId,
                'type' => 'directory',
            );
            $tableGateway = new TableGateway('media', $dbAdapter);
            $mediaTable = new MediaTable($tableGateway);
            $name = $mediaTable->getFileByNameAndPath($dirName, $path, $userId);
            if ($name) {
                $this->flashMessenger()
                    ->setNamespace('error')
                    ->addMessage('Folder with the same name already exists');
            } else {
                $mediaTable->saveFile($file);
                $this->flashMessenger()
                    ->setNamespace('success')
                    ->addMessage('Directory is created successfully');
            }
        } else {
            $this->flashMessenger()
                ->setNamespace('error')
                ->addMessage(current($inputFilter->getMessages('name')));
        }
        return $parentDir;
    }

    public function showFiles($uploadDir, $subDir, $dbAdapter)
    {
        $userSession = new Container('user');
        $userId = $userSession->offsetGet('userId');
        $tableGateway = new TableGateway('media', $dbAdapter);
        $mediaTable = new MediaTable($tableGateway);
        if (!$subDir) {
            $subDir = '/';
            $locationDir = null;
        } else {
            $locationDir = '/'. $subDir;
            $subDir = '/'. $subDir;
        }
        $handle = $mediaTable->getFilesByPath($subDir, $userId);
        $files = array();
        if ($handle) {
            foreach ($handle as $file) {
                if ($file['type'] == 'directory') {
                    $files[$file['name']] = array(
                        'location' => $locationDir .'/'. $file['name'],
                        'filesize' => $file['size'],
                        'mimetype' => $file['type'],
                        'name' => $file['name'],
                    );
                } else {
                    $files[$file['name']] = array(
                        'location' => '/upload/'. $userId .'/'. $file['name'],
                        'actionLocation' => $locationDir .'/'. $file['name'],
                        'filesize' => $file['size'],
                        'mimetype' => $file['type'],
                        'name' => $file['name'],
                    );
                }
            }
            return $files;
        } else {
            $parse = explode('/', $subDir);
            $name = array_pop($parse);
            $path = implode('/', $parse);
            if (count($parse) == 1) {
                $path = '/';
            }
            $check = $mediaTable->getFileByNameAndPath($name, $path, $userId);
            if ($check) {
                return $files;
            } else {
                $result = $mediaTable->getAllByUserId($userId);
                if (!$result) {
                    return $files;
                } else {
                    $this->flashMessenger()
                        ->setNamespace('error')
                        ->addMessage('Sorry, but this folder is not exist');
                    return 'error';
                }
            }
        }
    }

    public function getGoBackUri($subDir)
    {
        if (!$subDir) {
            $returnBackUri = '';
        } else {
            $path = explode('/', $subDir);
            if (count($path) == 1) {
                $returnBackUri = 'file-manager';
            } else {
                $hold = array_pop($path);
                $returnBackUri = implode('/', $path);
            }
        }
        return $returnBackUri;
    }
}
