<?php

namespace Users\Controller;

use Doctrine\ORM\Query;
use Users\Entity\Users;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

/**
 * Class UsersController
 * @package Users\Controller
 */
class UsersController extends AbstractRestfulController
{
    /**
     * @var \Doctrine\ORM\EntityManager instance of entity manager
     */
    protected $objectManager;

    /**
     * Get Instance of \Doctrine\ORM\EntityManager
     *
     * @return object
     */
    protected function getObjectManager()
    {
        if (!$this->objectManager) {
            $this->objectManager = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager')                ;
        }

        return $this->objectManager;
    }

    /**
     * Get users list
     *
     * @return JsonModel
     */
    public function getList()
    {
        $query = $this->getObjectManager()->createQuery('SELECT u FROM Users\Entity\Users u');
        $entity = $query->getResult(Query::HYDRATE_ARRAY);
        return new JsonModel(array('users' => $entity));
    }

    /**
     * Get user by id
     *
     * @param int $id
     * @return JsonModel
     */
    public function get($id)
    {
        try {
            $query = $this->getObjectManager()->createQuery('SELECT u FROM Users\Entity\Users u WHERE u.id = ?1');
            $query->setParameter(1, $id);
            $entity = $query->getSingleResult(Query::HYDRATE_ARRAY);
            return new JsonModel(array("user" => $entity));
        } catch (\Doctrine\ORM\NoResultException $e) {
            return $this->getResponse()->setStatusCode(Response::STATUS_CODE_404);
        }
    }

    /**
     * Create new user
     *
     * @param array $data
     * @return JsonModel
     */
    public function create($data)
    {
        try {
            $user = new Users();
            $user->setLogin($data['login']);
            $user->setEmail($data['email']);
            $this->getObjectManager()->persist($user);
            $this->getObjectManager()->flush();
            $userId = $user->getId();
            $this->getResponse()->getHeaders()->addHeaders(array(
                'Location' => '/users/' . $userId
            ));
            return $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
        } catch (\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return new JsonModel(array('error' => 'User with same login already exist'));
        }
    }

    /**
     * Update user entity
     *
     * @param int $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data)
    {
        $user = $this->getObjectManager()->find('Users\Entity\Users', $id);
        if (!$user) {
            return $this->getResponse()->setStatusCode(Response::STATUS_CODE_404);
        }
        $user->setLogin($data['login']);
        $user->setEmail($data['email']);
        $this->getObjectManager()->persist($user);
        $this->getObjectManager()->flush();
        return new JsonModel(array("user" => 'User updated'));
    }

    /**
     * Delete user entity
     *
     * @param int $id
     * @return JsonModel
     */
    public function delete($id)
    {
        $user = $this->getObjectManager()->find('Users\Entity\Users', $id);
        if (!$user) {
            return $this->getResponse()->setStatusCode(Response::STATUS_CODE_404);
        }
        $this->getObjectManager()->remove($user);
        $this->getObjectManager()->flush();
        return new JsonModel(array("user" => 'User deleted'));
    }
}
