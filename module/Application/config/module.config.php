<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'fbsignin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/users',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Users',
                        'action' => 'fbsignin',
                    )
                )
            ),
            'signin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/users/signin',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Users',
                        'action' => 'signin',
                    )
                )
            ),
            'fbsignup' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/users/fbsignup',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Users',
                        'action' => 'fbsignup',
                    )
                )
            ),
            'signup' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/users/signup',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Users',
                        'action' => 'signup',
                    )
                )
            ),
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'signout' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/signout',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Users',
                        'action'     => 'signout',
                    ),
                ),
            ),
            'activation' => array(
                'type' => 'segment',
                'options' => array(
                    'route'     => '/activation/code/[:num]/id/[:num1]',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Users',
                        'action'     => 'activation',
                    ),
                    'constraints' => array(
                        'num'    => '[a-zA-Z0-9]+',
                        'num1'    => '[0-9]+',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator'
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Users' => 'Application\Controller\UsersController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
    'auth' => array(
        'facebook' => array(
            'appId' => '597131957050480',
            'secret'=> '228d86ea64c27ec33d2849f1046b2a98'
        )
    ),
    'api' => array(
        'redirectUri'=> 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/users',
        'fbSignin'=> 'http://web-service.seryogin-ubnt.php.nixsolutions.com/fb/signin',
        'fbSignup'=> 'http://web-service.seryogin-ubnt.php.nixsolutions.com/fb/signup',
        'signin'=> 'http://web-service.seryogin-ubnt.php.nixsolutions.com/authentication/signin',
        'signup'=> 'http://web-service.seryogin-ubnt.php.nixsolutions.com/authentication/signup',
        'activation'=> 'http://web-service.seryogin-ubnt.php.nixsolutions.com/authentication/activation',
    ),
    'confirm' => array(
        'activation'=> 'http://web-service-zf2.seryogin-ubnt.php.nixsolutions.com/activation',
    )
);
