<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Form\FbSignupForm;
use Application\Form\FbSignupFilter;
use Application\Form\SigninForm;
use Application\Form\SigninFilter;
use Zend\Http\Client;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Application\Form\SignupForm;

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;

class UsersController extends AbstractActionController
{
    public function fbsigninAction()
    {
        $userSession = new Container('user');
        $userlogin = $userSession->offsetGet('login');
        if ($userlogin) {
            return $this->redirect()->toRoute('home');
        } else {
            $sessionManager = new SessionManager();
            $sessionManager->start();

            $config = $this->getServiceLocator()->get('Config');
            $options = $config['auth']['facebook'];

            // init app with app id and secret
            FacebookSession::setDefaultApplication($options['appId'], $options['secret']);

            // login helper with redirect_uri
            $helper = new FacebookRedirectLoginHelper($config['api']['redirectUri']);

            try {
                $session = $helper->getSessionFromRedirect();
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
            } catch(Exception $ex) {
                // When validation fails or other local issues
            }

            // see if we have a session
            if (isset($session)) {
                // graph api request for user data
                $request = new FacebookRequest($session, 'GET', '/me');
                $accessToken = $request->getParameters()['access_token'];

                $client = new Client();
                $client->setUri($config['api']['fbSignin']);
                $client->setMethod('POST');
                $client->setParameterPost(array(
                    'accessToken' => $accessToken,
                ));

                $response = $client->send();
                if ($response->isSuccess()) {
                    $result = json_decode($response->getContent());
                    if (!empty($result)) {
                        //save user in session
                        $userSession = new Container('user');
                        $userSession->login = $result->login;
                        return $this->redirect()->toRoute('home');
                    } else {
                        $response = $request->execute();
                        // get response
                        $data =  $response->getResponse();

                        $form = new FbSignupForm($data, $accessToken);
                        return new ViewModel(array('form'=>$form));
                    }
                }
            } else {
                $loginUrl  = $helper->getLoginUrl(array('email'));
                return $this->redirect()->toUrl($loginUrl);
            }
        }
    }
    public function signinAction()
    {
        $userSession = new Container('user');
        $userlogin = $userSession->offsetGet('login');
        if ($userlogin) {
            return $this->redirect()->toRoute('home');
        } else {
            $request = $this->getRequest();
            if (!$this->request->isPost()) {
                $form = new SigninForm();
                return new ViewModel(array('form'=>$form));
            }
            $post = $request->getPost();
            $form = new SigninForm();
            $inputFilter = new SigninFilter($post->login);
            $form->setInputFilter($inputFilter);
            $form->setData($post);

            if (!$form->isValid()) {
                $model = new ViewModel(array(
                    'error' => true,
                    'form' => $form,
                ));
                $model->setTemplate('application/users/signin');
                return $model;
            } else {

                $client = new Client();
                $config = $this->getServiceLocator()->get('Config');
                $client->setUri($config['api']['signin']);
                $client->setMethod('POST');
                $client->setParameterPost(array(
                    'username' => $form->getData()['login'],
                    'password' => $form->getData()['password'],
                ));
                $response = $client->send();

                if ($response->isSuccess()) {
                    $result = json_decode($response->getContent());
                    if ($result) {
                        $userSession = new Container('user');
                        $userSession->login = $result->login;
                        return $this->redirect()->toRoute('home');
                    }
                }
            }
        }
    }
    public function signupAction()
    {
        $request = $this->getRequest();
        if (!$this->request->isPost()) {
            $form = new SignupForm();
            return new ViewModel(array('form'=>$form));
        }
        $post = $request->getPost();
        $form = new SignupForm();
        $inputFilter = new FbSignupFilter();
        $form->setInputFilter($inputFilter);
        $form->setData($post);

        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('application/users/signup');
            return $model;
        } else {
            $this->register($form->getData());
            $model = new ViewModel(array(
                'complete' => true,
                'form' => $form,
            ));
            $model->setTemplate('application/users/signup');
            return $model;
        }
    }
    public function fbsignupAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->redirect()->toRoute('home');
        }
        $post = $request->getPost();
        $form = new FbSignupForm($post);
        $inputFilter = new FbSignupFilter();
        $form->setInputFilter($inputFilter);
        $form->setData($post);

        if (!$form->isValid()) {
            $model = new ViewModel(array(
                'error' => true,
                'form' => $form,
            ));
            $model->setTemplate('application/users/fbsignin');
            return $model;
        } else {
            $this->createUser($form->getData());
            $model = new ViewModel(array(
                'complete' => true,
                'form' => $form,
            ));
            $model->setTemplate('application/users/fbsignin');
            return $model;
        }
    }
    protected function register(array $data)
    {
        $client = new Client();
        $config = $this->getServiceLocator()->get('Config');
        $client->setUri($config['api']['signup']);
        $client->setMethod('POST');
        $client->setParameterPost(array(
            'uri' => $config['confirm']['activation'],
            'email' => $data['email'],
            'name' => $data['name'],
            'login' => $data['login'],
            'password' => $data['password'],
            'password2' => $data['password2'],
        ));

        $response = $client->send();

        if ($response->isSuccess()) {
            $result = json_decode($response->getContent());
            return $result;
        }
    }
    protected function createUser(array $data)
    {
        $client = new Client();
        $config = $this->getServiceLocator()->get('Config');
        $client->setUri($config['api']['fbSignup']);
        $client->setMethod('POST');
        $client->setParameterPost(array(
            'accessToken' => $data['accessToken'],
            'email' => $data['email'],
            'name' => $data['name'],
            'login' => $data['login'],
            'password' => $data['password'],
            'password2' => $data['password2'],
        ));

        $response = $client->send();

        if ($response->isSuccess()) {
            $result = json_decode($response->getContent());
            return $result;
        }
    }

    public function signoutAction()
    {
        $userSession = new Container('user');
        $userSession->offsetUnset('login');
        return $this->redirect()->toRoute('home');
    }

    public function activationAction()
    {
        $code = $this->getEvent()->getRouteMatch()->getParam('num');
        $userId = $this->getEvent()->getRouteMatch()->getParam('num1');
        $config = $this->getServiceLocator()->get('Config');
        $client = new Client($config['api']['activation']);
        $client->setParameterGet(array(
            'id' => $userId,
            'code' => $code,
        ));

        $response = $client->send();

        if ($response->isSuccess()) {
            $result = json_decode($response->getContent());
            if (!$result) {
                return new ViewModel();
            } else {
                return new ViewModel(array(
                    'error' => true,
                ));
            }
        }
    }
}
