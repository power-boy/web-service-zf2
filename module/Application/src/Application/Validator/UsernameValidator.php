<?php
/**
 * Taras Seryogin
 * Date: 5/14/14
 * Time: 4:29 PM
 */
namespace Application\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Http\Client;

class UsernameValidator extends AbstractValidator
{
    const NOT_FOUND = 404;
    const LOGIN = 'login';
    protected $messageTemplates = array(
        self::LOGIN => "User not found"
    );
    public function isValid($value)
    {

        $client = new Client();
        $client->setUri('http://web-service.seryogin-ubnt.php.nixsolutions.com/authentication/signin');
        $client->setMethod('POST');
        $client->setParameterPost(array(
            'username' => $value,
        ));

        $response = $client->send()->getStatusCode();
        if ($response == self::NOT_FOUND) {
            $this->error(self::LOGIN);
            return false;
        }
        return true;
    }
}
