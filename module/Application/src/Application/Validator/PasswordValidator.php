<?php
/**
 * Taras Seryogin
 * Date: 5/14/14
 * Time: 4:29 PM
 */
namespace Application\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Http\Client;

class PasswordValidator extends AbstractValidator
{
    const BAD_REQUEST = 400;
    const FORBIDDEN = 403;
    const PASSWORD = 'password';
    const ACTIVATION = 'activation';
    protected $messageTemplates = array(
        self::PASSWORD => "Wrong password",
        self::ACTIVATION => "Your account is pending activation",
    );

    public function __construct($login)
    {
        parent::__construct();
        $this->login = $login;
    }
    public function isValid($value)
    {
        $client = new Client();
        $client->setUri('http://web-service.seryogin-ubnt.php.nixsolutions.com/authentication/signin');
        $client->setMethod('POST');
        $client->setParameterPost(array(
            'username' => $this->login,
            'password' => $value,
        ));

        $response = $client->send()->getStatusCode();
        if ($response == self::BAD_REQUEST) {
            $this->error(self::PASSWORD);
            return false;
        }
        if ($response == self::FORBIDDEN) {
            $this->error(self::ACTIVATION);
            return false;
        }
        return true;

    }
}
