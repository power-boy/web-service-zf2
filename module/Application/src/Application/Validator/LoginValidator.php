<?php
/**
 * Taras Seryogin
 * Date: 5/14/14
 * Time: 4:29 PM
 */
namespace Application\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Http\Client;

class LoginValidator extends AbstractValidator
{
    const LOGIN = 'login';
    protected $messageTemplates = array(
        self::LOGIN => "User with login '%value%' already exists"
    );
    public function isValid($value)
    {

        $client = new Client();
        $client->setUri('http://web-service.seryogin-ubnt.php.nixsolutions.com/fb/check-user-exist');
        $client->setMethod('POST');
        $client->setParameterPost(array(
            'login' => $value,
        ));

        $response = $client->send();

        if ($response->isSuccess()) {
            $result = json_decode($response->getContent());
            if (!empty($result->errors->login)) {
                $this->error(self::LOGIN, $value);
                return false;
            }

            return true;
        }
    }
}