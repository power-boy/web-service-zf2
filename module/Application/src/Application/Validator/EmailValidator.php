<?php
/**
 * Taras Seryogin
 * Date: 5/14/14
 * Time: 4:29 PM
 */
namespace Application\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Http\Client;

class EmailValidator extends AbstractValidator
{
    const EMAIL = 'email';
    protected $messageTemplates = array(
        self::EMAIL => "User with email '%value%' already exists"
    );
    public function isValid($value)
    {

        $client = new Client();
        $client->setUri('http://web-service.seryogin-ubnt.php.nixsolutions.com/fb/check-user-exist');
        $client->setMethod('POST');
        $client->setParameterPost(array(
            'email' => $value,
        ));

        $response = $client->send();

        if ($response->isSuccess()) {
            $result = json_decode($response->getContent());
            if (!empty($result->errors->email)) {
                $this->error(self::EMAIL, $value);
                return false;
            }

            return true;
        }
    }
}