<?php
/**
 * Signup Filter
 *
 * Taras Seryogin
 */
namespace Application\Form;

use Application\Validator\EmailValidator;
use Application\Validator\LoginValidator;
use Zend\InputFilter\InputFilter;

class FbSignupFilter extends InputFilter
{
    public function __construct()
    {

        //login
        $this->add(array(
            'name' => 'login',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 100,
                    ),
                ),
                new LoginValidator()
            ),
        ));

        //name
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 100,
                    ),
                ),
            ),
        ));

        //email
        $this->add(array(
            'name' => 'email',
            'required' => true,

            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'EmailAddress',
                    'options' => array(
                        'domain' => true,
                    ),
                ),
                new EmailValidator()
            ),
        ));

        //password
        $this->add(array(
            'name' => 'password',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 6,
                        'max'      => 128,
                    ),
                ),
            ),
        ));

        //password2
        $this->add(array(
            'name' => 'password2',
            'required' => true,
            'filters' => array(
              array(
                  'name' => 'StringTrim'
              ),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array( 'min' => 6 ),
                ),
                array(
                    'name' => 'identical',
                    'options' => array('token' => 'password' )
                ),
            ),
        ));
    }
}
