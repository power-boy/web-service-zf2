<?php
/**
 * Signup Form
 *
 * Taras Seryogin
 */
namespace Application\Form;

use Zend\Form\Form;

class SignupForm extends Form
{
    public function __construct()
    {
        parent::__construct('Signup');
        $this->setAttribute('method', 'post');


        //login
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'login',
            ),
            'options' => array(
                'label' => 'login',
            ),
        ));

        //name
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'name',
            ),
            'options' => array(
                'label' => 'name',
            ),
        ));

        //email
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'email',
            ),
            'options' => array(
                'label' => 'email',
            ),
        ));

        //password
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'password',
            ),
            'options' => array(
                'label' => 'password',
            ),
        ));

        //password2
        $this->add(array(
            'name' => 'password2',
            'attributes' => array(
                'type' => 'password',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'password2',

            ),
            'options' => array(
                'label' => 'repeat password',
            ),
        ));

        //submit
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'register',
                'class'  => 'btn btn-primary'
            ),
        ));

    }
}
