<?php
/**
 * Signup Form
 *
 * Taras Seryogin
 */
namespace Application\Form;

use Zend\Form\Form;

class SigninForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('Signin');
        $this->setAttribute('method', 'post');


        //login
        $this->add(array(
            'name' => 'login',
            'attributes' => array(
                'type' => 'text',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'login',
            ),
            'options' => array(
                'label' => 'login',
            ),
        ));


        //password
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'required' => 'required',
                'class'  => 'form-control',
                'placeholder' => 'password',
            ),
            'options' => array(
                'label' => 'password',
            ),
        ));

        //submit
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Sign In',
                'class'  => 'btn btn-primary'
            ),
        ));

    }
}
