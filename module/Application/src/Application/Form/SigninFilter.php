<?php
/**
 * Signup Filter
 *
 * Taras Seryogin
 */
namespace Application\Form;

use Application\Validator\UsernameValidator;
use Application\Validator\PasswordValidator;
use Zend\InputFilter\InputFilter;

class SigninFilter extends InputFilter
{
    public function __construct($check)
    {

        //login
        $this->add(array(
            'name' => 'login',
            'required' => true,
            'filters'  => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 1,
                        'max'      => 100,
                    ),
                ),
                new UsernameValidator()
            ),
        ));


        //password
        $this->add(array(
            'name' => 'password',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim'
                ),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min'      => 6,
                        'max'      => 128,
                    )
                ),
                new PasswordValidator($check)
            )
        ));
    }
}
