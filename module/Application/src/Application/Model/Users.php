<?php
/**
 * Taras Seryogin
 * Date: 5/13/14
 * Time: 11:31 AM
 */
namespace Application\Model;

class Users
{
    public $id;
    public $fbid;
    public $login;
    public $name;
    public $email;
    public $password;

    public function setPassword($clear_password)
    {
        $this->password = md5($clear_password);
    }

    public function exchangeArray($data)
    {
        $this->fbid = (isset($data['fbid'])) ?
            $data['fbid'] : null;
        $this->login = (isset($data['login'])) ?
            $data['login'] : null;
        $this->name = (isset($data['name'])) ?
            $data['name'] : null;
        $this->email = (isset($data['email'])) ?
            $data['email'] : null;
        if (isset($data["password"])) {
            $this->setPassword($data["password"]);
        }
    }
}
