<?php
/**
 * Taras Seryogin
 * Date: 5/13/14
 * Time: 11:31 AM
 */
namespace Application\Model;

use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Form;
use Zend\Validator\Db\RecordExists;

class UsersTable
{
    protected $tableGateway;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function saveUser(Users $user)
    {
        $data = array(
//            'fbid' => $user->fbid,
            'login' => $user->login,
            'email' => $user->email,
            'name' => $user->name,
//            'password' => $user->password,
        );

        $sql = "SELECT * FROM users WHERE `login` = '".$user->login."' or `email` ='". $user->email."'";
        $statement = $this->tableGateway->getAdapter()->query($sql);
        $result = $statement->execute();


        $validator = new RecordExists(
            array(
                'table' => 'users',
                'field' => 'email',
                'adapter' => $this->tableGateway->getAdapter()
            )
        );

        if ($validator->isValid($user->email)) {
            $validator->setMessage('User already exist');
        }


//
//        if (!empty($result->current()['login'])) {
//             throw new \Exception("User with same login already exist");
//        }
//        if (!empty($result->current()['email'])) {
//            throw new \Exception("User with same email already exist");
//        }

//        $this->tableGateway->insert($data);
    }
    public function getUser($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
}