<?php
/**
 * Created by PhpStorm.
 * User: seryogin
 * Date: 9/18/14
 * Time: 12:44 PM
 */

interface SocialMediaAdapter
{
    public function post($msg);
}

class Facebook
{
    public function postToWall($msg)
    {
        echo 'Posting message';
    }
}


class FacebookAdapter implements SocialMediaAdapter
{
    private $facebook;
    public function __construct(Facebook $facebook)
    {
        $this->facebook = $facebook;
    }
    public function post($msg)
    {
        $this->facebook->postToWall($msg);
    }
}
$facebook = new FacebookAdapter(new Facebook());
$facebook->post('bla bla bla');