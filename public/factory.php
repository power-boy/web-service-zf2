<?php
/**
 * Created by PhpStorm.
 * User: seryogin
 * Date: 9/18/14
 * Time: 1:55 PM
 */

interface Shape
{
    public function draw();
}

class Position {};

class Rectagle implements Shape
{
    private $position;

    public function __construct($pos)
    {
        $this->position = $pos;
    }

    public function draw()
    {
        echo 'Drawing a rectangle';
    }
}

class ShapeFactory
{
    public function create($type)
    {
        if ($type == 'Rectangle') {
            return new Rectagle((new Position()));
        }
    }
}

$factory = new ShapeFactory();
$rect = $factory->create('Rectangle');
echo $rect->draw();