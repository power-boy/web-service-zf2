define(['better', 'better/Panel'],function($,Panel){

    var FileManager;


    /**
     * @class FileManager
     * @extends Panel
     */
     FileManager = $.declare('better.FileManager', Panel, {
        //template: template,
        objectProp: {},
        file: null,
        name: null,
        type: null,
        size: null,
        download: null,
        delete: null,
        //templateInMarkup: true,

        __construct: function() {

            //call parent::__construct
            this.inherited(arguments)
        },
        render: function(){
            //call parent::render
            this.inherited(arguments);
            //render widget template here
        },


        addFile: function(newFile) {
            //console.log(file);
            var newRow = '<tr>' +
                '<td class="col-md-6"><a class="text-info" data-toggle="lightbox" href="'+ newFile.previewlocation +'">'+ newFile.name +'</a></td>' +
                '<td class="col-md-3">'+ newFile.type +'</td>' +
                '<td class="col-md-2">'+ newFile.size +'</td>' +
                '<td class="col-md-1">' +
                '<div class="dropdown conf-align">' +
                '<button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">' +
                '<span class="glyphicon glyphicon-cog"></span>' +
                '</button>' +
                '<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">'+
                '<li role="presentation"><a role="menuitem" tabindex="-1" href="'+'/file-manager/download/'+ newFile.actionlocation +'.ext" class="btn btn-info btn-xs">Download</a></li>'+
                '<li role="presentation"><a role="menuitem" tabindex="-1" href="'+'/file-manager/delete/'+ newFile.actionlocation +'.ext" class="btn btn-danger btn-xs">Delete</a></li>'+
                '</ul>'+
                '</div>' +
                '</td>' +
                '</tr>';
            var nodeCheck = $('#myTable tbody tr');
            if (nodeCheck.length > 0) {
                $('#myTable tbody tr:last').after(newRow);
            } else {
                $('#myTable tbody').html(newRow);
            }
        }

    });

    $.ns.setObject('better.FileManager', FileManager);
    return FileManager



});