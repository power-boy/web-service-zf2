require.config({
    baseUrl: '/js/',
    paths: {
        jquery: './jquery',
        position: './better/ui/position',
        'dropzone-lib': './dropzone-amd-module',
        'ekko-lightbox': './ekko-lightbox',
        'jquery.tablesorter': './jquery.tablesorter',
         bootstrap: './bootstrap'
    },
    packages: [{
        name: 'better',
        location: 'better',
        main: 'better'
    }],
    shim: {
        'jquery.tablesorter': {
            deps: ["jquery"],
            exports: "jQuery.tablesorter"
        },
        'ekko-lightbox': {
            deps: ["jquery"],
            exports: "$.fn.ekkoLightbox"
        },
        'bootstrap': {
            deps:['jquery']
        },
        position: ["jquery"]
    }
});

require(['better', 'main', 'dropzone-file-manager']);