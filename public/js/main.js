define(['jquery', 'bootstrap', 'ekko-lightbox', 'jquery.tablesorter'],function($){
    $(document).ready(function () {
        $('.create-dir-form').submit(function(event){
            event.preventDefault();
            var name = $('#folder-name').val();
            var dir = $('#uploadDir').val();
            if (dir) {
                url = "/create-dir/" + dir +'/'+ name;
            } else {
                url = "/create-dir/" + name;
            }
            window.location = url;
        });

        $(".dropzone input[type=hidden]").val($('#uploadDir').val());

        $(".info").animate({opacity: 1.0}, 1500).fadeOut("slow");

        $("#myTable").tablesorter({ headers: { 1: { sorter: false}, 3: {sorter: false} } });

        $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    });
});
