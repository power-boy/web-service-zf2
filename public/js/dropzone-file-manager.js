define(['dropzone-lib', 'better/FileManager'],function(Dropzone, FileManager){
    $(document).ready(function () {
        var myDropzone = new Dropzone('#my-awesome-dropzone', {
            url:'/file-manager/upload',
            clickable: true,
            method: 'post',
            maxFiles: 10,
            parallelUploads: 3,
            maxFilesize: 50,
            addRemoveLinks: true,
            dictRemoveFile: 'Remove',
            dictCancelUpload: 'Cancel',
            dictCancelUploadConfirmation: 'Confirm cancel?',
            dictDefaultMessage: 'Drop files here to upload',
            dictFallbackMessage: 'Your browser does not support drag n drop file uploads',
            dictFallbackText: 'Please use the fallback form below to upload your files like in the olden days',
            paramName: 'file',
            forceFallback: false,
            createImageThumbnails: true,
            maxThumbnailFilesize: 1,
            autoProcessQueue: true
        });
        Dropzone.autoDiscover = false;

        var FileManagerDemo = new FileManager();
        myDropzone.on("success", function(file, xhr) {
            $(".not-found").remove();
            var newFile = xhr.file;
            FileManagerDemo.addFile(newFile);
        });
    });
});
